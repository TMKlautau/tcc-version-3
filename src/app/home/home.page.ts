import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import * as math from 'mathjs'

declare let audioinput:any;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {
	
	public;
	
	constructor(public plt: Platform){
		this.plt.ready().then((readySource) => {
		  console.log('Platform ready from', readySource);
		  // Platform now ready, execute any required native code
		  audioinput.initialize();
		});
		
		const fftWorker = new Worker('./assets/workers/fftWorker.js');
		
		fftWorker.onmessage = (event) =>{
			let auxTimeWRK:number = performance.now();
			this.auxTimeWRK500 += auxTimeWRK - this.auxTimeLastWRK;
			this.auxTimeLastWRK = auxTimeWRK;			
			this.auxConterWRK++;
			if(this.auxConterWRK===500){
				this.auxTimeWRK500 = this.auxTimeWRK500/500;
				console.log("average time between last 500 worker calls: ", this.auxTimeWRK500)
				this.auxTimeWRK500 = 0;
				this.auxConterWRK = 0;
			}
			this.lastFFTCalculated = event.data;
			for(let i = 0; i < 16; i++){
				this.auxData[i] += this.lastFFTCalculated[i];
			}
			this.fftAverageCounter++;
			if(this.fftAverageCounter == this.fftAverageWindow){
				for(let i = 0; i < 16; i++){
				this.fftAverage[i] = this.auxData[i]/this.fftAverageWindow;
				this.auxFFTBars[i] = this.fftAverage[i]>this.dBCutOff ? 'O' : 'I';
				}
				this.auxData = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
				this.fftAverageCounter = 0;
			}		
			this.timeSincePlay += 0.002;
			//console.log(event);
		};
		
		addEventListener('audioinput', (evt) => {
			//console.log("newEvent: ", this.auxConterEVT, " -- " , performance.now());
			let auxTimeEVT:number = performance.now();
			this.auxTimeEVT500 += auxTimeEVT - this.auxTimeLastEVT;
			this.auxTimeLastEVT = auxTimeEVT;			
			this.auxConterEVT++;
			if(this.auxConterEVT===500){
				this.auxTimeEVT500 = this.auxTimeEVT500/500;
				console.log("average time from last 500 events: ", this.auxTimeEVT500)
				this.auxTimeEVT500 = 0;
				this.auxConterEVT = 0;
			}
			
			this.auxData2 = (<any>evt).data;
			fftWorker.postMessage((<any>evt).data);
		}, false);
	}
	
	capture(){
		this.recordingFlag = true;
		this.statusRecording = "Recording";
		this.recordingSymbol = "square";
		this.timeSincePlay = 0;
		audioinput.start({sampleRate: 16000, bufferSize: 32 , normalize: false});
	}
	
	togleRecording(){
		if(this.recordingFlag){
			this.recordingFlag = false;
			this.statusRecording = "Off";
			this.recordingSymbol = "play";
			audioinput.stop();
		}else{
			audioinput.checkMicrophonePermission((hasPermission)=>{
				if(hasPermission) {
					console.log("We already have permission to record.");
					this.capture();
				}else{
					audioinput.getMicrophonePermission((hasPermission, message)=>{
						if (hasPermission) {
							console.log("User granted us permission to record.");
							this.capture();
						} else {
							console.warn("User denied permission to record.");
						}
					});
				}
			});
		}	
	}

	private;
	auxConterEVT:number = 0;
	auxTimeLastEVT:number = 0;
	auxTimeEVT500:number = 0;
	auxCounterFFT:number = 0;
	auxTime500FFT:number = 0;
	auxConterWRK:number = 0;
	auxTimeLastWRK:number = 0;
	auxTimeWRK500:number = 0;
	
	
	
	recordingFlag:boolean = false;
	statusRecording:string = "Off";
	recordingSymbol:string = "play";
	auxData:number[] = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
	auxData2:any[] = [];
	fftAverage:number[] = [];
	fftAverageCounter:number = 0;
	lastFFTCalculated:any[] = [];
	timeSincePlay:number = 0;
	
	fftAverageWindow:number = 50;
	
	auxFFTBars:string[] = ['I','I','I','I','I','I','I','I','I','I','I','I','I','I','I','I'];
	
	dBCutOff:number = 37;
	
	
}
 